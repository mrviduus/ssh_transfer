import time
from datetime import datetime
import paramiko
import os
import logging
from pathlib import Path
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool





#auth
user = 'nik'
pwd = 'askue'
port = 22


#ssh = paramiko.SSHClient()
#ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

#log
logger = logging.getLogger(__name__)

logger.setLevel(logging.INFO)

handler = logging.FileHandler('logger.log')

handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

handler.setFormatter(formatter)

logger.addHandler(handler)

print("Before you start, make sure, the file: 'IPs.txt' was created in the root directory of the script\n")
print("The file should contain a list with IPs separated by a single space")
input("Press any key to continue, or ctrl + c for cancel")


print ('Enter the path \n(default: /home/nik/)')
input_path = input()

if (input_path == ""): 
    input_path = "/home/nik/"

file = open('IPs.txt', 'r')#файл с ip после каждого ip -пробел

print ('Enter local directory \n(default: files/)')
input_local = input()

if (input_local == ""):
    input_local = "files/"



for line in file:
    timestamps = str(datetime.now())
    print ('Timestamp:',timestamps)
    logger.info('start')
    info = {}
    info['ip'] = line.split(' ')[0]
       

    try:
        ip_log = 'Connection to IP: ' + info['ip']
        print (ip_log)
        logger.info(ip_log)

        #ssh.connect(info['ip'], username = user, password = pwd, port = port, timeout = 15)


        transport = paramiko.Transport((info['ip'], port))
        transport.connect(username = user, password = pwd)
        print ('Connection is success!')
        logger.info('Connection is success!')
        
        #sftp = ssh.open_sftp()
        print ('opening sftp')

        sftp = paramiko.SFTPClient.from_transport(transport)
        print ('sftp open')
        logger.info('sftp open')
        #cmd = ssh.invoke_shell()
        print ('Go to local folder')
        sourse_folder = input_local
        print ('Successful')
        
        inbourd = os.listdir(sourse_folder)



        print (inbourd)



        
        for folder_file in inbourd:
            local_path = sourse_folder + folder_file
            print (local_path)
            
            file_path = input_path + folder_file
            try:
                print ("check file")
                print(sftp.stat(input_path + folder_file))
                print('File exist')
                sftp.put(local_path, file_path)
                print('File was rewrited')
            except IOError:
                print ('Uploading file')
                sftp.put(local_path, file_path)
                print ('File was uploaded')
                

        
        logger.info(local_path)

        #close

        sftp.close()
        transport.close()

        time.sleep(3)
        print ('Done!')

        print ('_____________________________________________')
        print ('\n')
        print ('\n')
        print ('\n')

        logger.info('Done!')

    except Exception as e:
        error_log = str(e)
        print ('ошибка: ' + error_log + '/n' )
        logger.info(error_log)

file.close()