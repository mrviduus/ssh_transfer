# -*- coding: utf-8 -*-
import threading
from queue import Queue
import time
from datetime import datetime
import paramiko
import os
import logging
from pathlib import Path
from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool






#auth
user = ''
pwd = ''
port = 22


#ssh = paramiko.SSHClient()
#ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

#log
logger = logging.getLogger(__name__)

logger.setLevel(logging.INFO)

handler = logging.FileHandler('logger.log')

handler.setLevel(logging.INFO)


formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

handler.setFormatter(formatter)

logger.addHandler(handler)

succsess_conn = []
unsuccsess_conn = []

ip_amount = 0 # ips fo queu


class Uploading(threading.Thread):
    

    
    def __init__(self, queue):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.queue = queue
    
    def run(self):
        """Запуск потока"""
        while True:
            # Получаем  из очереди
            line = self.queue.get()
            
            # Upload file
            self.upload_file(line)
            
            # Отправляем сигнал о том, что задача завершена
            self.queue.task_done()
 
    def upload_file(self, line):
        
            timestamps = str(datetime.now())
            print ('Timestamp:',timestamps)
            
            info = {}
            info['ip'] = line.split(' ')[0]


            try:
                ip_log = 'Connection to IP: ' + info['ip']
                print (ip_log)
                logger.info(ip_log)

                #ssh.connect(info['ip'], username = user, password = pwd, port = port, timeout = 15)

                transport = paramiko.Transport((info['ip'], port))
                transport.connect(username = user, password = pwd)
              
                print ('Connection is success! '  + info['ip'])
                logger.info('Connection is success! '  + info['ip'])

                #sftp = ssh.open_sftp()
             
                sftp = paramiko.SFTPClient.from_transport(transport)
                
                #cmd = ssh.invoke_shell()
                
                sourse_folder = input_local
                
                inbourd = os.listdir(sourse_folder)

                print (inbourd)

                for folder_file in inbourd:
                    local_path = sourse_folder + folder_file
                    print (local_path)

                    file_path = input_path + folder_file
                    try:
                        
                        print(sftp.stat(input_path + folder_file))
                        print('File exist '  + info['ip'])
                        sftp.put(local_path, file_path)
                        
                        
                    except IOError:
                        
                        sftp.put(local_path, file_path)
                        print ('File was uploaded '  + info['ip'])
                        
                #close

                sftp.close()
                transport.close()

                
                print ('Done! '  + info['ip'])             

                
                logger.info('Done!'  + info['ip'])

            except Exception as e:
                error_log = str(e)
                print ('Error: ' + error_log + '/n' )
                
                logger.info(error_log)

       
 
def main(file):
    """
    Start app
    """
    print(ip_amount)
    queue = Queue()
    # Запускаем поток и очередь
    for ip in range(ip_amount):
        t = Uploading(queue)
        t.setDaemon(True)
        t.start()
    
    # Даем очереди нужные нам ссылки для загрузки
    for ip in file:
        queue.put(ip)

    #report()
    # Ждем завершения работы очереди
    queue.join()
    input("Press Enter to continue...")

    

#Reports  todo report
#def report():
#    for ips in succsess_conn:
#        print("Succsess connections")
#        print(ips)
#    
#    for fail_ip in unsuccsess_conn:
#        print("Connections with fails")
#        print(fail_ip)



#DELETE
def delete_files():
    print ('What directory you want clear? \n(default: /home/nik/)')
    input_path = input()

    user = input("Enter username: ")
    pwd = input("Enter password: ")

    if (input_path == ""): 
        input_path = "/home/nik/"

    file = open('IPs.txt', 'r')#файл с ip после каждого ip -пробел

    print ('Directory with file what you want remove \n(default: files/)')
    input_local = input()

    if (input_local == ""):
        input_local = "files/"
    for line in file:
        timestamps = str(datetime.now())
        print ('Timestamp:',timestamps)
        logger.info('start')
        info = {}
        info['ip'] = line.split(' ')[0]


        try:
            ip_log = 'Connection to IP: ' + info['ip']
            print (ip_log)
            logger.info(ip_log)

            #ssh.connect(info['ip'], username = user, password = pwd, port = port, timeout = 15)


            transport = paramiko.Transport((info['ip'], port))
            transport.connect(username = user, password = pwd)
            print ('Connection is success!')
            logger.info('Connection is success!')

            #sftp = ssh.open_sftp()
            print ('opening sftp')

            sftp = paramiko.SFTPClient.from_transport(transport)
            print ('sftp open')
            logger.info('sftp open')
            #cmd = ssh.invoke_shell()
            print ('Go to local folder')
            sourse_folder = input_local
            print ('Successful')

            inbourd = os.listdir(sourse_folder)



            print (inbourd)




            for folder_file in inbourd:
                local_path = sourse_folder + folder_file
                print (local_path)

                #file_path = input_path + folder_file
                try:
                    print ("check file")
                    print(sftp.stat(input_path + folder_file))
                    print('File exist')
                    print (folder_file)
                    sftp.remove(folder_file)
                    print('File was removed')
                except IOError:
                    print ('Nothing to remove')
                    


        
            logger.info(local_path)

            #close

            sftp.close()
            transport.close()

            time.sleep(3)
            print ('Done!')

            print ('_____________________________________________')
            print ('\n')


            logger.info('Done!')

        except Exception as e:
            error_log = str(e)
            print ('Error: ' + error_log + '/n' )
            logger.info(error_log)
    
        





#Application is start

print ("Choose options:\n 0)Add 1)Delete \n Default == 0")
input_choose_opt = input()


if input_choose_opt =="1":
    print("Delete files")

    delete_files()



if input_choose_opt == "0" or input_choose_opt == "":
    print("Before you start, make sure, the file: 'IPs.txt' was created in the root directory of the script\n")
    print("The file should contain a list with IPs separated by a single space")
    input("Press any key to continue, or ctrl + c for cancel")
    
    user = input("Enter username: ")
    pwd = input("Enter password: ")

    print ('Enter the path \n(default: /home/nik/)')
    input_path = input()

    if (input_path == ""): 
        input_path = "/home/nik/"

    try:
        file_ips = open('IPs.txt', 'r')#todo to find best way   
        for i in file_ips:
            ip_amount += 1
        file_ips.close()
    except:
        print('You must create IPs.txt')

    file = open('IPs.txt', 'r')#файл с ip после каждого ip -пробел



    print ('Enter local directory \n(default: files/)')
    input_local = input()

    if (input_local == ""):
        input_local = "files/"
        main(file)

