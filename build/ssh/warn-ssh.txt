
This file lists modules PyInstaller was not able to find. This does not
necessarily mean this module is required for running you program. Python and
Python 3rd-party packages include a lot of conditional or optional module. For
example the module 'ntpath' only exists on Windows, whereas the module
'posixpath' only exists on Posix systems.

Types if import:
* top-level: imported at the top-level - look at these first
* conditional: imported within an if-statement
* delayed: imported from within a function
* optional: imported within a try-except-statement

IMPORTANT: Do NOT post this list to the issue-tracker. Use it as a basis for
           yourself tracking down the missing module. Thanks!

missing module named 'multiprocessing.forking' - imported by c:\users\vasiliy.vdovichenko\appdata\local\programs\python\python37-32\lib\site-packages\PyInstaller\loader\rthooks\pyi_rth_multiprocessing.py (optional)
missing module named multiprocessing.get_context - imported by multiprocessing (top-level), multiprocessing.pool (top-level), multiprocessing.managers (top-level), multiprocessing.sharedctypes (top-level)
missing module named multiprocessing.TimeoutError - imported by multiprocessing (top-level), multiprocessing.pool (top-level)
missing module named multiprocessing.BufferTooShort - imported by multiprocessing (top-level), multiprocessing.connection (top-level)
missing module named multiprocessing.AuthenticationError - imported by multiprocessing (top-level), multiprocessing.connection (top-level)
missing module named multiprocessing.set_start_method - imported by multiprocessing (top-level), multiprocessing.spawn (top-level)
missing module named multiprocessing.get_start_method - imported by multiprocessing (top-level), multiprocessing.spawn (top-level)
missing module named resource - imported by posix (top-level), D:\python\ssh\ssh.py (top-level)
missing module named posix - imported by os (conditional, optional), D:\python\ssh\ssh.py (top-level)
missing module named _posixsubprocess - imported by subprocess (conditional), multiprocessing.util (delayed), D:\python\ssh\ssh.py (top-level)
missing module named readline - imported by cmd (delayed, conditional, optional), code (delayed, conditional, optional), pdb (delayed, optional), D:\python\ssh\ssh.py (top-level)
excluded module named _frozen_importlib - imported by importlib (optional), importlib.abc (optional), D:\python\ssh\ssh.py (top-level)
missing module named _frozen_importlib_external - imported by importlib._bootstrap (delayed), importlib (optional), importlib.abc (optional), D:\python\ssh\ssh.py (top-level)
missing module named _winreg - imported by platform (delayed, optional), D:\python\ssh\ssh.py (top-level)
missing module named _scproxy - imported by urllib.request (conditional)
missing module named java - imported by platform (delayed), D:\python\ssh\ssh.py (top-level)
missing module named 'java.lang' - imported by platform (delayed, optional), D:\python\ssh\ssh.py (top-level), xml.sax._exceptions (conditional)
missing module named vms_lib - imported by platform (delayed, conditional, optional), D:\python\ssh\ssh.py (top-level)
missing module named termios - imported by tty (top-level), getpass (optional), D:\python\ssh\ssh.py (top-level)
missing module named urllib.unquote - imported by urllib (conditional), asn1crypto._iri (conditional)
missing module named urllib.quote - imported by urllib (conditional), asn1crypto._iri (conditional)
missing module named grp - imported by shutil (optional), tarfile (optional), pathlib (delayed), D:\python\ssh\ssh.py (top-level)
missing module named org - imported by pickle (optional), D:\python\ssh\ssh.py (top-level)
missing module named pwd - imported by posixpath (delayed, conditional), shutil (optional), tarfile (optional), http.server (delayed, optional), webbrowser (delayed), getpass (delayed), pathlib (delayed, conditional, optional), D:\python\ssh\ssh.py (top-level), netrc (delayed, conditional)
missing module named 'org.python' - imported by copy (optional), D:\python\ssh\ssh.py (top-level), xml.sax (delayed, conditional)
missing module named multiprocessing.Pool - imported by multiprocessing (top-level), D:\python\ssh\ssh.py (top-level)
missing module named cStringIO - imported by asn1crypto.core (conditional), paramiko.py3compat (conditional)
missing module named copy_reg - imported by cStringIO (top-level)
missing module named __builtin__ - imported by paramiko.py3compat (conditional)
runtime module named six.moves - imported by cryptography.x509.general_name (top-level)
missing module named UserDict - imported by asn1crypto._ordereddict (conditional)
missing module named urlparse - imported by asn1crypto._iri (conditional)
missing module named StringIO - imported by six (conditional)
missing module named thread - imported by paramiko.win_pageant (optional)
missing module named fcntl - imported by paramiko.agent (delayed)
missing module named sspi - imported by paramiko.ssh_gss (optional)
missing module named sspicon - imported by paramiko.ssh_gss (optional)
missing module named pywintypes - imported by paramiko.ssh_gss (optional)
missing module named gssapi - imported by paramiko.ssh_gss (optional)
